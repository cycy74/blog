from django.shortcuts import render
from django.http import HttpResponse
from articles.models import Article

def index(request):
    return HttpResponse( '<h1> Bienvenue sur mon super blog </h1>')

def home(request):
    return render (request, 'articles/accueil.html')

def  article_list(request):
    liste_des_articles = Article.objects.all()
    context = {
        "articles":liste_des_articles,
    }
    return render(request, "articles/article_list.html", context)

def article_detail(request, article_id):
    un_article= Article.objects.get (id=article_id)
    return render (request, "articles/article_view.html", { "article":un_article } )