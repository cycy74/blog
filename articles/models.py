from django.db import models

# Create your models here.
class Article(models.Model):
    titre = models.CharField(max_length=255)
    contenu = models.TextField(null=True, blank=True)
    auteur = models.CharField(max_length=50)
    date_de_creation = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.titre