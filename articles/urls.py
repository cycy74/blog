from articles.views import home, article_detail, article_list,index
from django.urls import path
urlpatterns = [
    path('',index),
    path('accueil', home, name="accueil"),
    path('article/', article_list,name="articles"),
    path('articles/<int:article_id>', article_detail, name="article_detail"),
]
